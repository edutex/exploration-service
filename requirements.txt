# General
django==3.1.2
jsonfield==3.1.0
# Static Files
whitenoise==5.2.0

# Websocket
channels==2.4.0
channels-redis==3.1.0
asgiref==3.2.10
django-channels-presence==1.0.0
websocket_client==0.57.0

# Timed Tasks
apscheduler==3.6.3
django-apscheduler==0.4.2
redis==3.5.3

# Pruning Channels
django-celery-beat==2.0.0

# MySQL
mysqlclient==2.0.1

# REST Framework
djangorestframework==3.12.1
markdown==3.2.2
django-filter==2.4.0
djangorestframework-simplejwt==4.4.0

# Logging
sentry-sdk==0.18.0

# Kafka
confluent-kafka==1.5.0

#Environ
django-environ

#Import Export
django-import-export