from django.urls import path
from .views import *
from django.contrib.auth.views import LogoutView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_simplejwt.views import TokenRefreshView # new

app_name = "authentification"

urlpatterns = [
    # http://host/..URLS
    path('login/', login_view, name="login"),
    path('register/', register_user, name="register"),
    path('logout/', logout_user, name="logout"),

    # API
    path('api/sign_up/', APISignUpView.as_view(), name='sign_up'),
    path('api/log_in/', APILogInView.as_view(), name='log_in'),  # new
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),  # new


    # Rest-API ~ Testing
    path('helloworld/', HelloView.as_view(), name="hello"),
    path('api-token-auth/', obtain_auth_token, name='api_token_auth')
]