from import_export import resources
from .models import *


class StudyResource(resources.ModelResource):
    class Meta:
        model = Study


class MircoSurveyAnswersResource(resources.ModelResource):
    class Meta:
        model = MircoSurveyAnswers