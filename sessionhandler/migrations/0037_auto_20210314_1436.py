# Generated by Django 3.1.2 on 2021-03-14 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sessionhandler', '0036_auto_20210314_1427'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='requestsensors',
            name='delay_microphone',
        ),
        migrations.AlterField(
            model_name='requestsensors',
            name='delay_heartbeat',
            field=models.CharField(blank=True, choices=[('0', 'SENSOR_DELAY_FASTEST'), ('1', 'SENSOR_DELAY_GAME'), ('3', 'SENSOR_DELAY_NORMAL'), ('2', 'SENSOR_DELAY_UI')], default='3', max_length=56, null=True),
        ),
    ]
