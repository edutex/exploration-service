# Generated by Django 3.1.2 on 2021-03-14 13:32

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sessionhandler', '0029_auto_20210314_1331'),
    ]

    operations = [
        migrations.AlterField(
            model_name='requestsensors',
            name='ambientlight',
            field=models.CharField(blank=True, choices=[('request', 'Request the Sensor'), ('do_not_request', 'Dont request the Sensor')], default='do_not_request', max_length=56, null=True),
        ),
        migrations.AlterField(
            model_name='requestsensors',
            name='bluetooth',
            field=models.CharField(blank=True, choices=[('request', 'Request the Sensor'), ('do_not_request', 'Dont request the Sensor')], default='do_not_request', max_length=56, null=True),
        ),
        migrations.AlterField(
            model_name='requestsensors',
            name='gps',
            field=models.CharField(blank=True, choices=[('request', 'Request the Sensor'), ('do_not_request', 'Dont request the Sensor')], default='do_not_request', max_length=56, null=True),
        ),
        migrations.AlterField(
            model_name='requestsensors',
            name='wifi',
            field=models.CharField(blank=True, choices=[('request', 'Request the Sensor'), ('do_not_request', 'Dont request the Sensor')], default='do_not_request', max_length=56, null=True),
        ),
    ]
