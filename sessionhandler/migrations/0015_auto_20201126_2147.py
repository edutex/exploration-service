# Generated by Django 3.1.2 on 2020-11-26 20:47

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('sessionhandler', '0014_auto_20201126_2126'),
    ]

    operations = [
        migrations.CreateModel(
            name='MicroSurveyDatabase',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, max_length=256, null=True)),
                ('study', models.CharField(blank=True, max_length=256, null=True)),
                ('type', models.CharField(blank=True, max_length=256, null=True)),
                ('schedule', models.CharField(blank=True, max_length=256, null=True)),
                ('sessionID', models.CharField(blank=True, max_length=256, null=True)),
                ('androidID', models.CharField(blank=True, max_length=256, null=True)),
                ('userID', models.CharField(blank=True, max_length=256, null=True)),
                ('timestamp_question_asked', jsonfield.fields.JSONField()),
                ('timestamp_question_answered', models.CharField(blank=True, max_length=256, null=True)),
                ('identifier', models.CharField(blank=True, max_length=256, null=True)),
                ('question', models.CharField(blank=True, max_length=256, null=True)),
                ('answers', models.CharField(blank=True, max_length=256, null=True)),
                ('user_answer', models.CharField(blank=True, max_length=256, null=True)),
            ],
            options={
                'verbose_name_plural': 'XZ DB Micro Surveys',
            },
        ),
        migrations.AlterModelOptions(
            name='databasenotification',
            options={'verbose_name_plural': 'XZ DB Notifications'},
        ),
        migrations.AlterModelOptions(
            name='databaserequestsensor',
            options={'verbose_name_plural': 'XZ DB RequestSensors'},
        ),
        migrations.AlterModelOptions(
            name='databasesurvey',
            options={'verbose_name_plural': 'XZ DB Survey'},
        ),
        migrations.AlterModelOptions(
            name='databasesurveyquestions',
            options={'verbose_name_plural': 'XZ DB Questions'},
        ),
        migrations.AlterModelOptions(
            name='microsurvey',
            options={'verbose_name_plural': 'XA Micro Surveys'},
        ),
        migrations.AlterModelOptions(
            name='microsurveyanswers',
            options={'verbose_name_plural': "XF Micro Survey A's"},
        ),
        migrations.AlterModelOptions(
            name='microsurveyquestion',
            options={'verbose_name_plural': "XE Micro Survey Q's"},
        ),
        migrations.AlterModelOptions(
            name='notification',
            options={'verbose_name_plural': 'XC Notifications'},
        ),
        migrations.AlterModelOptions(
            name='participant',
            options={'verbose_name_plural': 'X Participant'},
        ),
        migrations.AlterModelOptions(
            name='requestsensors',
            options={'verbose_name_plural': 'XD Sensor Requests'},
        ),
        migrations.AlterModelOptions(
            name='session',
            options={'verbose_name_plural': 'X Sessions'},
        ),
        migrations.AlterModelOptions(
            name='study',
            options={'verbose_name_plural': 'X Studies'},
        ),
        migrations.AlterModelOptions(
            name='survey',
            options={'verbose_name_plural': 'XB Surveys'},
        ),
        migrations.AlterModelOptions(
            name='surveyquestion',
            options={'verbose_name_plural': "XG Survey Q's"},
        ),
        migrations.AddField(
            model_name='microsurvey',
            name='remindAfter',
            field=models.CharField(choices=[(0, '0'), (1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5'), (6, '6'), (7, '7'), (8, '8'), (9, '9'), (10, '10'), (11, '11'), (12, '12'), (13, '13'), (14, '14'), (15, '15'), (16, '16'), (17, '17'), (18, '18'), (19, '19'), (20, '20'), (21, '21'), (22, '22'), (23, '23'), (24, '24'), (25, '25'), (26, '26'), (27, '27'), (28, '28'), (29, '29'), (30, '30'), (31, '31'), (32, '32'), (33, '33'), (34, '34'), (35, '35'), (36, '36'), (37, '37'), (38, '38'), (39, '39'), (40, '40'), (41, '41'), (42, '42'), (43, '43'), (44, '44'), (45, '45'), (46, '46'), (47, '47'), (48, '48'), (49, '49')], default='0', help_text='Renotify if question was not answered after x Min/Sec', max_length=5),
        ),
    ]
