from django.contrib.auth.decorators import login_required
from django.shortcuts import render


# Create your views here.

def create_room(request):
    return render(request, 'sessionhandler/create_room.html')


def room(request, room_name):
    return render(request, 'sessionhandler/view_room.html', {'room_name': room_name})

