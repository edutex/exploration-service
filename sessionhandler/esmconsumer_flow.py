from channels_presence.models import Room
import logging
from django.utils.timezone import now
from datetime import timedelta



logger = logging.getLogger(__name__)
EXPIRATION_DELTA_TIME_SESSION = 1800  # Seconds
def is_session_empty(session):
    if session == "":
        return True
    else:
        return False

def is_participant_unregistered(participant):
    if participant == "":
        return True
    else:
        return False


def update_time(session):
    session.expiration = now() + timedelta(seconds=EXPIRATION_DELTA_TIME_SESSION)
    session.save()
    return session
