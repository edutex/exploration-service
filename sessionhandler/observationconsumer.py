from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from channels_presence.signals import presence_changed
from django.dispatch import receiver
from channels_presence.models import Presence
import json
import logging

logger = logging.getLogger(__name__)


class ObservationConsumer(WebsocketConsumer):
    """ This Class supports the index.html observation of active sessionhandler connections """

    def connect(self):
        self.room_name = self.scope['url_route']['kwargs']
        self.room_group_name = 'observation'

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )
        self.accept()

    def disconnect(self, close_code):
        # Leave room group
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name,
            self.channel_name
        )
        logger.info("Observation consumer left.")

    # Receive message from WebSocket
    def receive(self, text_data):
        pass


    def send_log(self, event):
        self.send(event["message"])

