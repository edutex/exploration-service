from django.contrib.auth.models import User
from sessionhandler.models import *
import logging
import uuid
import datetime

from .import_teststudy import *
from .remove_teststudy import *
from .evaluate import *
from .socket_test import *

USE_LOGGER = False
RUN_TIME_MIN = 20


def inform(text: str):
    if USE_LOGGER:
        logger.info(text)
    else:
        print(text)


def run_init():
    delete_debug_models()
    create_user()
    import_debug_entries()
    inform("Models should be imported")


def run_delete():
    delete_debug_models()
    inform("Models should be deleted")


def run_study(studyname=None, run_time_min=None, delete_after=False):

    if run_time_min is None:
        run_time = RUN_TIME_MIN * 60
    else:
        run_time = run_time_min * 60

    run_init()
    if studyname is None:
        studies = Study.objects.all()
        for elem in studies:
            if elem.id < 0:
                used_study = elem
            break

    else:
        studies = Study.objects.filter(name=studyname)
        if studies:
            used_study = studies[0]
            participants = Participant.objects.all()
            if participants:
                for participant in participants:
                    if participant.id < 0:
                        participant.study = used_study
                        participant.save()
                        break


        else:
            print("No study with this name found.")
            return


    inform("Session runs for " + str(run_time))
    run_websocket(run_time)

    time.sleep(5)
    evaluate(run_time/60, used_study)


    if delete_after:
        run_delete()
