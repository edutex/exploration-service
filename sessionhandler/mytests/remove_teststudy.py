from sessionhandler.models import *
from sessionarchive.models import *
from django.contrib.auth.models import User

MODELS = [RequestSensors,
          MicroSurvey,
          MicroSurveyQuestion,
          MicroSurveyAnswers,

          Notification,
          SurveyQuestion,
          Survey,
          Study,
          Participant]

DBMODELS = [DatabaseRequestSensor,
            MicroSurveyDatabase,
            DatabaseNotification]


def delete_debug_models():
    for elem in SessionArchive.objects.all():
        if elem.target_address == "socket_loremipsum":
            elem.delete()
    print("Deleted SessionArchive")

    for elem in Session.objects.all():
        if elem.participant.identifier == "TestIdentifier":
            elem.delete()
    print("Deleted Sessions")

    for model in MODELS + [User]:
        for elem in model.objects.all():
            if elem.id < 0:
                try:
                    elem.delete()
                except:
                    pass

    print("Deleted Sessionhandler Models")
    for model in DBMODELS:
        for elem in model.objects.all():
            if elem.userID == "TestWebsocket":
                elem.delete()

    for elem in DatabaseSurvey.objects.all():
        if elem.id < 0 or elem.androidID == "TestWebsocket":
            query = DatabaseSurveyQuestions.objects.filter(survey=elem)
            for obj in query:
                obj.delete()
            elem.delete()

    print("Deleted Sessionhandler DB Models")
