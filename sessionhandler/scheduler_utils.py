from datetime import timedelta, datetime
import random


# Functions to add time attributes to scheduled tasks
def add_minutes(minutes_to_add):
    execution_time = datetime.utcnow() + timedelta(hours=1, minutes=minutes_to_add)
    return execution_time.strftime("%Y/%m/%d %H:%M:%S")


def add_seconds(seconds_to_add):
    execution_time = datetime.utcnow() + timedelta(hours=1, seconds=seconds_to_add)
    return str(execution_time)


def add_minutes_for_cron(minutes_to_add):
    if minutes_to_add == 0:
        execution_time = datetime.utcnow() + timedelta(hours=0, minutes=minutes_to_add, seconds=2)
    else:
        execution_time = datetime.utcnow() + timedelta(hours=0, minutes=minutes_to_add)

    return (execution_time.strftime("%H"), execution_time.strftime("%M"), execution_time.strftime("%S"),
            execution_time.strftime("%Y"), execution_time.strftime("%m"), execution_time.strftime("%d"),)


def add_seconds_for_cron(seconds_to_add):
    execution_time = datetime.utcnow() + timedelta(hours=0, seconds=seconds_to_add)
    return (execution_time.strftime("%H"), execution_time.strftime("%M"), execution_time.strftime("%S"),
            execution_time.strftime("%Y"), execution_time.strftime("%m"), execution_time.strftime("%d"),)


def get_execution_time(selected_schedule):
    return add_minutes_for_cron(selected_schedule)


# Functions to add sequences to scheduled tasks

def get_sequence(objects: list, sequence: str, randomize: bool):
    if randomize:
        random.shuffle(objects)
        return objects

    elif sequence == "" or sequence is None:
        return objects

    elif sequence != "":
        sequence = [int(i.strip()) for i in sequence.split(",") if i.strip().isnumeric()]
        ordered = []

        for pos in sequence:
            q_obj = objects.filter(id=pos)
            if q_obj:
                ordered.append(q_obj[0])

        missing_objs = []
        for obj in objects:
            if not obj in ordered:
                missing_objs.append(obj)

        return ordered + missing_objs


def get_text_answers(text_answers: str):
    answers = text_answers.split(",")
    answers = [i.strip() for i in answers]
    return answers


def get_numerical_answers(lower: int, upper: int):
    return [i for i in range(lower, upper + 1)]


def time_segments(period, num_questions, style, buffer=2):

    if num_questions == 0:
        return []

    segment_period = int(period) / num_questions
    segments = [(i * segment_period, (i + 1) * segment_period) for i in range(0, num_questions)]

    schedules = []
    if style == "random":
        if segment_period > buffer:
            for segment in segments:
                start = int(segment[0])
                end = int(segment[1])
                end -= buffer

                randomness = random.randint(start, end)
                schedules.append(randomness)
        else:
            for segment in segments:
                start = int(segment[0])
                end = int(segment[1])
                randomness = random.randint(start, end)
                schedules.append(randomness)
    else:
        for segment in segments:
            schedules.append(segment[0])

    return schedules