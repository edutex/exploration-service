import json
import logging
from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer
from channels.layers import get_channel_layer
from channels_presence.models import Presence
from django.utils.timezone import now
from datetime import datetime
import json
import uuid
from .scheduler import register_questionnaires, prune_scheduled_tasks, register_sensor_requests, register_micro_surveys, \
    recover_scheduled_tasks
from .scheduler_parser import survey_parser, sensor_request_parser
from .esmconsumer_db import *
from .esmconsumer_flow import *
from .scheduler import register_questionnaires, prune_scheduled_tasks, register_sensor_requests
from .kafka_producer import KafkaProducer
from sessionarchive.models import SessionArchive

logger = logging.getLogger(__name__)

# Communication Constants
ACTION_REQUEST_STATE_PARTICIPANT = "action_request_state_participant"
ACTION_REQUEST_SESSION_STATE = "action_request_session_state"
ACTION_RECOVER_LAST_SESSION = "action_recover_last_session"
ACTION_RECOVER_LAST_SESSION_IF_AVAILABLE = "action_recover_last_session_if_available"
ACTION_PRE_SESSION_SURVEY = "action_pre_session_surveys"
ACTION_START_SESSION = "action_start_session"
ACTION_POST_SESSION_SURVEY = "action_post_session"
ACTION_FORCE_STOP_SESSION = "action_force_stop"
CLIENT_RESPONSE = "client_response"
HEARTBEAT = "heartbeat"


class ESMConsumer(JsonWebsocketConsumer):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # self.producer = KafkaProducer()

    def connect(self):

        self.scope['participant'] = self.get_paticipant(headers=dict(self.scope['headers']))
        self.room_name = self.scope['url_route']['kwargs']['room_name']
        self.room_group_name = 'socket_%s' % self.room_name

        # Join room group
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name,
            self.channel_name
        )

        self.accept()
        logger.debug("Inside Connect")
        if self.scope['user'].is_anonymous:
            msg_user_error = {
                'isError': True,
                'user-error': 'unknown'
            }
            self.send(json.dumps(msg_user_error))
            self.close()
        elif self.scope['participant'] == "":
            msg_participant_error = {
                'isError': True,
                'participant-error': 'unknown'
            }
            self.send(json.dumps(msg_participant_error))
            self.close()

        else:
            self.scope['esm_session'] = ""
            Room.objects.add(self.room_group_name, self.channel_name, self.scope["user"])
            logger.info("User joined room: " + str(self.room_group_name) + " " + str(self.channel_name))
            # prune_scheduled_tasks(self.room_group_name)

    def disconnect(self, close_code):
        # TODO CLOSE CODE ?

        if self.scope['user'].is_anonymous:
            pass

        elif self.scope['participant'] == None:
            pass

        else:
            Room.objects.remove(self.room_group_name, self.channel_name)
            logger.debug("User left room: " + str(self.room_group_name) + " " + str(self.channel_name))

            async_to_sync(self.channel_layer.group_discard)(
                self.room_group_name,
                self.channel_name
            )
            # prune_scheduled_tasks(self.room_group_name)

    def receive_json(self, content, **kwargs):

        """Function is called when message from sessionhandler is received"""
        if is_participant_unregistered(self.scope['participant']):
            print("return 03")
            return

        self.redirect_receive_observation(content)

        if ACTION_REQUEST_STATE_PARTICIPANT in content:
            """q_participants_session = Session.objects.filter(participant=self.scope['participant'], terminated=False)
            if len(q_participants_session) == 0:
                logger.debug("No active session found. Sending information to handheld")
                ret = {
                    'participant_finished': True,
                }
            else:
                logger.debug("Active Session found. Sending information to handheld")
                ret = {
                    'participant_finished': False,
                }

            self.send(json.dumps(ret))
            self.redirect_send_observation(json.dumps(ret))"""
            pass
        elif ACTION_RECOVER_LAST_SESSION in content:
            """prune_scheduled_tasks(target_address=self.room_group_name)
            q_participants_session = Session.objects.filter(participant=self.scope['participant'],
                                                            terminated=False)
            if content[ACTION_RECOVER_LAST_SESSION]:

                json_session = json.loads(q_participants_session.reverse()[0].toJson())
                json_session['isRecovery'] = True
                self.send(json.dumps(json_session))
                self.redirect_send_observation(json.dumps(json_session))
                self.scope['esm_session'] = q_participants_session.reverse()[0]
                recover_scheduled_tasks(target_address=self.room_group_name, tasks=self.scope["esm_session"].timetable)
                # register_questionnaires(target_address=self.room_group_name, study=self.scope['participant'].study)
                # register_sensor_requests(target_address=self.room_group_name, study=self.scope['participant'].study)
                # register_micro_surveys(target_address=self.room_group_name, study=self.scope['participant'].study)
                logger.debug("Recovered last not terminated session.")

            else:
                for session in q_participants_session:
                    session.terminated = True
                    session.save()"""
            pass

        elif ACTION_RECOVER_LAST_SESSION_IF_AVAILABLE in content:
            time_now = now()
            q_participants_session = Session.objects.filter(participant=self.scope['participant'], terminated=False)
            if len(q_participants_session) > 0:
                session_obj = q_participants_session.reverse()[0]
                if (session_obj.expiration - time_now).total_seconds() > 0:
                    json_session = json.loads(session_obj.toJson())
                    json_session['isRecovery'] = True
                    self.scope['esm_session'] = update_time(session_obj)
                    self.send(json.dumps(json_session))
                    self.redirect_send_observation(json.dumps(json_session))
                else:
                    prune_scheduled_tasks(self.room_group_name)
                    logger.debug("No active session found. Sending information to handheld")
                    ret = {
                        'participant_finished': True,
                        'isRecovery': False,
                    }
                    self.send(json.dumps(ret))
                    self.redirect_send_observation(json.dumps(ret))
            else:
                prune_scheduled_tasks(self.room_group_name)
                logger.debug("No active session found. Sending information to handheld")
                ret = {
                    'participant_finished': True,
                    'isRecovery': False,
                }
                self.send(json.dumps(ret))
                self.redirect_send_observation(json.dumps(ret))

        elif ACTION_FORCE_STOP_SESSION in content:

            logger.info("Force Stop called")

            try:
                self.scope['esm_session'] = update_time(self.scope['esm_session'])
            except:
                pass
            current_session = self.scope['esm_session']

            if not is_session_empty(current_session):
                current_session.ending = str(datetime.utcnow().isoformat()[:-3] + 'Z')
                current_session.terminated = True
                current_session.save()
                self.scope['esm_session'] = ""
                logger.info("session ended successfully.")


        elif ACTION_PRE_SESSION_SURVEY in content:
            if not is_session_empty(self.scope['esm_session']):
                logger.debug("Found not terminated Session. But Handheld requests Pre-Survey")
                print("return 02")
                return

            try:
                self.scope["participant"] = Participant.objects.get(id=self.scope["participant"].id)
                self.scope["participant"].study.refresh_from_db()
                logger.info("Refreshed Participant and participants Study from DB.")
            except Exception as er:
                logger.error("ERROR WHILE REFRESHING")
                return
                pass
            self.scope["tmp_esm_session"] = create_new_session(participant=self.scope['participant'], is_tmp=True)

            q_pre_survey = Survey.objects.filter(study=self.scope['participant'].study, type="pre_survey")
            if len(q_pre_survey) == 0:
                ret = {
                    'name': "fake pre survey",
                    'fake': 'pre_survey',
                    'isSurvey': True,
                    'type': 'pre_survey',
                }
                self.send_json(ret)
                self.redirect_send_observation(json.dumps(ret))
                logger.info("Sended fake pre survey")
            else:
                ret = survey_parser(survey=q_pre_survey[0], study=self.scope['participant'].study)
                self.send_json(ret)
                self.redirect_send_observation(
                    json.dumps(survey_parser(survey=q_pre_survey[0], study=self.scope['participant'].study)))
                logger.info("Sended Pre Survey to Handheld")

            self.add_to_archive(ret)

            q_pre_requests = RequestSensors.objects.filter(study=self.scope['participant'].study, type="pre_request")
            for request in q_pre_requests:
                rew = sensor_request_parser(request=request, study=self.scope['participant'].study)
                self.send_json(rew)
                self.add_to_archive(rew)

        elif ACTION_START_SESSION in content:
            logger.info("Starting new Session..")
            logger.info("Participant info " + self.scope['participant'].name)
            # prune_scheduled_tasks(target_address=self.room_group_name)
            logger.info("Created new Session")

            session = self.scope["tmp_esm_session"]
            session = update_time(session)
            session.start = str(datetime.utcnow().isoformat()[:-3] + 'Z')
            session.target_address = self.room_group_name
            session.save()

            self.scope['esm_session'] = session
            self.send(self.scope['esm_session'].toJson())
            self.redirect_send_observation(self.scope['esm_session'].toJson())

            register_questionnaires(target_address=self.room_group_name, study=self.scope['participant'].study)
            register_sensor_requests(target_address=self.room_group_name, study=self.scope['participant'].study)
            register_micro_surveys(target_address=self.room_group_name, study=self.scope['participant'].study)
        elif ACTION_POST_SESSION_SURVEY in content:
            prune_scheduled_tasks(self.room_group_name)
            if is_session_empty(self.scope['esm_session']):
                print("return 01")
                return

            cur_session = self.scope['esm_session']
            cur_session.requested_post = True
            cur_session.ending = str(datetime.utcnow().isoformat()[:-3] + 'Z')

            q_post_survey = Survey.objects.filter(study=self.scope['participant'].study, type="post_survey")
            if len(q_post_survey) == 0:
                ret = {
                    'name': "fake post survey",
                    'fake': 'post_survey',
                    'isSurvey': True,
                    'type': 'post_survey'}
                self.send_json(ret)
                # self.redirect_send_observation(json.dumps(ret))
                cur_session.terminated = True
                database_sync_to_async(cur_session.save())
                self.scope['esm_session'] = ""
            else:
                ret = survey_parser(q_post_survey[0], self.scope['participant'].study)
                self.send_json(ret)
                database_sync_to_async(cur_session.save())
                self.scope['esm_session'] = cur_session

            self.add_to_archive(ret)

            q_pre_requests = RequestSensors.objects.filter(study=self.scope['participant'].study, type="post_request")
            for request in q_pre_requests:
                req = sensor_request_parser(request=request, study=self.scope['participant'].study)
                self.send_json(req)
                self.add_to_archive(req)
                self.redirect_send_observation(
                    json.dumps(sensor_request_parser(request=request, study=self.scope['participant'].study)))

        elif CLIENT_RESPONSE in content:
            Presence.objects.touch(self.channel_name)
            client_response = content['client_response']
            try:
                self.scope['esm_session'] = update_time(self.scope['esm_session'])
            except:
                pass
            current_session = self.scope['esm_session']

            if "isSurvey" in client_response:
                if client_response['isSurvey']:

                    if client_response['type'] == "microSurvey":
                        save_micro_survey(client_response)
                        return

                    if client_response['type'] == "pre_survey":
                        try:
                            client_response['sessionID'] = self.scope["tmp_esm_session"].sessionID
                        except:
                            pass

                    save_survey(client_response)

                    if not is_session_empty(current_session):
                        if client_response["type"] == "pre_survey":
                            current_session.androidID = client_response["androidID"]
                            current_session.save()
                            self.scope['esm_session'] = current_session
                        elif client_response["type"] == "post_survey":
                            current_session.terminated = True
                            current_session.save()
                            self.scope['esm_session'] = ""
                            logger.debug("session ended successfully.")

                else:
                    save_notification(client_response)

            elif "isRequest" in client_response:
                if client_response['type'] == "pre_request":
                    try:
                        client_response['sessionID'] = self.scope["tmp_esm_session"].sessionID
                    except:
                        pass
                save_sensor_request(client_response)

            print("pre-producer call")
            # Publish the events to Kafka
            # self.producer.produce("testKey", client_response)

        elif HEARTBEAT in content:
            Presence.objects.touch(self.channel_name)

    def chat_message(self, event):
        """Function is called when sessionhandler receives message from room group"""
        message = event['message']
        self.send_json(message)
        logger.debug("Function chat_message is called. Message: " + str(message))

        self.add_to_archive(message)

    def remove_scheduled(self, event):
        if self.scope["esm_session"] != "":
            session = self.scope["esm_session"]
            id = event["message"]
            if id in session.timetable:
                if session.timetable[id]['type'] == "single":
                    removed = session.timetable.pop(id, None)
                    logger.debug("Session Removed: " + str(removed))
                    session.save()
                    self.scope["esm_session"] = session
                else:
                    logger.debug("Session did not removed: is interval")

    def add_scheduled_to_session(self, dict_messages):
        if self.scope["esm_session"] != "":
            session = self.scope["esm_session"]
            if type(session.timetable) == type(None):
                session.timetable = {}

            print(dict_messages)

            for key in dict_messages.keys():
                session.timetable[key] = dict_messages[key]

            session.save()
            self.scope["esm_session"] = session
            logger.debug("Added scheduled task(s) to session.")

    def get_paticipant(self, headers):
        try:
            participant = Participant.objects.filter(user=self.scope['user'])[0]
        except:
            participant = ""
        return participant

    def redirect_receive_observation(self, message):
        async_to_sync(get_channel_layer().group_send)('observation',
                                                      {'type': 'send.log',
                                                       'message': json.dumps({'destination': 'receive-log',
                                                                              'target-address': self.room_group_name,
                                                                              'study': self.scope[
                                                                                  'participant'].study.name,
                                                                              'message': message,
                                                                              })}
                                                      )

    def redirect_send_observation(self, message):
        async_to_sync(get_channel_layer().group_send)('observation',
                                                      {'type': 'send.log',
                                                       'message': json.dumps({'destination': 'send-log',
                                                                              'target-address': self.room_group_name,
                                                                              'study': self.scope[
                                                                                  'participant'].study.name,
                                                                              'message': message,
                                                                              })}
                                                      )

    def add_to_archive(self, message):
        try:

            sArchive = SessionArchive(
                participant=self.scope['participant'],
                target_address=self.room_group_name,
                message=json.loads(message)
            )
            sArchive.save()
        except:
            sArchive = SessionArchive(
                participant=self.scope['participant'],
                target_address=self.room_group_name,
                message=message
            )
            sArchive.save()
