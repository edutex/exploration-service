from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DefaultUserAdmin
from .models import *
from import_export.admin import ImportExportModelAdmin


# GENERAL MODELS


class StudyAdmin(ImportExportModelAdmin):
    pass


admin.site.register(Study, StudyAdmin)

class ParticipantAdmin(ImportExportModelAdmin):
    pass
admin.site.register(Participant, ParticipantAdmin)
admin.site.register(Session)


# ESM MODELS
## Surveys
class SurveyStudiesInline(admin.TabularInline):
    model = Study.surveys.through
    extra = 0


class SurveyQuestionsInline(admin.StackedInline):
    model = SurveyQuestion.survey.through
    extra = 0


class SurveyAdmin(ImportExportModelAdmin):
    fieldsets = [
        ("General", {'fields': ['name', 'type', 'display_order_sequence']}),
        ("Only applies for Type 'Interval' and 'Fixed time' ", {'fields': ['offset']}),
        ('Only applies for Type "Interval"', {'fields': ['schedule']}),
    ]

    inlines = [SurveyStudiesInline, SurveyQuestionsInline, ]


admin.site.register(Survey, SurveyAdmin)


class SurveyQuestionsAdmin(ImportExportModelAdmin):
    pass


admin.site.register(SurveyQuestion, SurveyQuestionsAdmin)


class DatabaseSurveyQuestionsInline(admin.StackedInline):
    model = DatabaseSurveyQuestions
    extra = 0


class DatabaseSurveyAdmin(admin.ModelAdmin):
    inlines = [DatabaseSurveyQuestionsInline, ]


admin.site.register(DatabaseSurvey, DatabaseSurveyAdmin)


## uSurveys

class MicroSurveyQuestionsInline(admin.TabularInline):
    model = MicroSurveyQuestion.microsurvey.through
    extra = 0


class MicroSurveyAnswersInline(admin.TabularInline):
    model = MicroSurveyAnswers.microsurveyquestion.through
    extra = 0
    max_num = 1


class MicroSurveyStudyInline(admin.TabularInline):
    model = Study.microsurvey.through
    extra = 0


class MicroSurveyAdmin(ImportExportModelAdmin):
    fieldsets = [
        ("General", {'fields': ['name', 'type']}),
        ("Schedule General", {'fields': ['offset', 'schedule', 'questions_schedule']}),
        ("Questions Sequence", {'fields': ["randomize", "display_order_sequence"]}),
        ("Questions Attributes", {'fields': ["questionLifetime", "remindOnceAfter"]})
    ]
    inlines = [MicroSurveyQuestionsInline, MicroSurveyStudyInline]


class MicroSurveyQuestionAdmin(ImportExportModelAdmin):
    fieldsets = [
        ("General", {'fields': ["question"]})
    ]
    inlines = [MicroSurveyAnswersInline]


class MicroSurveyAnswersAdmin(ImportExportModelAdmin):
    pass


admin.site.register(MicroSurvey, MicroSurveyAdmin)
admin.site.register(MicroSurveyQuestion, MicroSurveyQuestionAdmin)
admin.site.register(MicroSurveyAnswers, MicroSurveyAnswersAdmin)
admin.site.register(MicroSurveyDatabase)


## Notifications
class NotificationStudiesInline(admin.TabularInline):
    model = Study.notifications.through
    extra = 0


class NotificationAdmin(ImportExportModelAdmin):
    fieldsets = [

        ("General", {'fields': ['name', 'type', 'offset', 'schedule', ]}),
        ("Q&A", {'fields': ['question', 'add_answers_allowed', 'text_answer',
                            'numerical_answer_lower', 'numerical_answer_upper']}),

    ]
    inlines = [NotificationStudiesInline, ]


admin.site.register(Notification, NotificationAdmin)

admin.site.register(DatabaseNotification)


# REQUEST SENSORS
class RequestSensorsStudiesAdmin(admin.TabularInline):
    model = Study.sensor_requests.through
    extra = 0


class RequestsSensorsNotificationAdmin(admin.TabularInline):
    model = Notification.sensorrequest.through
    extra = 0
    max_num = 1


class RequestsSensorsSurveyAdmin(admin.TabularInline):
    model = Survey.sensorrequest.through
    extra = 0
    max_num = 1
    verbose_name = "Questionnaire"
    verbose_name_plural = "Questionnaires"


class RequestsSensorsMicroSurveyAdmin(admin.TabularInline):
    model = MicroSurvey.sensorrequest.through
    extra = 0
    max_num = 1
    verbose_name = "Micro Questionnaire"
    verbose_name_plural = "Micro Questionnaires"


class RequestSensorsAdmin(ImportExportModelAdmin):
    fieldsets = [

        ("General", {'fields': ['name', 'type', 'offset']}),
        ("Only for Type Interval", {'fields': ['schedule']}),
        ("Request data from the position sensors", {'fields': ['non_physical_environment_data']}),
        ("Request data about the physical learning environment",
         {'fields': ['physical_environment_data',
                     'recording_time_physical_environment_data',
                     "fidelity_physical_environment_data"]}),

        ("Requests behavioral data",
         {'fields': ['behavioral_data',
                     'recording_time_behavioral_data',
                     'fidelity_behavioral_data']}),

        ("Request data from the physiological sensors",
         {'fields': ['physiological_data',
                     'recording_time_physiological_data',
                     'fidelity_physiological_data']}),

    ]
    inlines = [RequestSensorsStudiesAdmin, RequestsSensorsNotificationAdmin, RequestsSensorsSurveyAdmin,
               RequestsSensorsMicroSurveyAdmin]


admin.site.register(RequestSensors, RequestSensorsAdmin)
admin.site.register(DatabaseRequestSensor)
