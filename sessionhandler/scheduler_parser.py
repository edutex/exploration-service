from .models import *
from .scheduler_utils import *
import uuid
import json


def notification_parser(notification, study, event_id=""):
    if event_id == "":
        event_id = str(uuid.uuid4())

    if notification.text_answer is not None:
        answers = get_text_answers(notification.text_answer)
    else:
        answers = get_numerical_answers(notification.numerical_answer_lower, notification.numerical_answer_upper)

    ret = {
        'name': notification.name,
        'study': study.getIdentifier(),
        'event_id': event_id,
        'isSurvey': False,
        'type': notification.type,
        'schedule': notification.schedule,
        'question': notification.question,
        'answers': answers,
        'add_answers_allowed': notification.add_answers_allowed,
        'notification_id': notification.id,
    }

    return ret


def survey_parser(survey, study, event_id=""):
    if event_id == "":
        event_id = str(uuid.uuid4())

    survey_questions = SurveyQuestion.objects.filter(survey=survey)

    sequence = get_sequence(objects=survey_questions, sequence=survey.display_order_sequence, randomize=False)

    questions = []
    for question in sequence:

        if question.text_answer is not None:
            answers = get_text_answers(question.text_answer)
        else:
            answers = get_numerical_answers(question.numerical_answer_lower, question.numerical_answer_upper)

        tmp = {
            'display_order': int(question.display_order),
            'display_type': question.display_type,
            'question': question.question,
            'answers': answers,
            'add_answers_allowed': question.add_answers_allowed,
            'question_id': question.id,
        }
        questions.append(tmp)

    for i in range(1, len(questions)+1):
       questions[i-1]["display_order"] = i

    ret = {
        'name': survey.name,
        'study': study.getIdentifier(),
        'event_id': event_id,
        'isSurvey': True,
        'type': survey.type,
        'schedule': survey.schedule,
        'survey_questions': questions,
        'survey_id': survey.id,
    }

    return ret


def sensor_request_parser(request, study, event_id):
    ret = {
        'isRequest': True,
        'sensorrequest_id': request.id,
        'event_id': event_id,
        'name': request.name,
        'study': study.getIdentifier(),
        'type': request.type,
        'schedule': request.schedule,

        "non_physical_environment_data": request.non_physical_environment_data,

        "physical_environment_data": request.physical_environment_data,
        "recording_time_physical_environment_data": int(request.recording_time_physical_environment_data),
        "fidelity_physical_environment_data": request.fidelity_physical_environment_data,

        "behavioral_data": request.behavioral_data,
        "recording_time_behavioral_data": int(request.recording_time_behavioral_data),
        "fidelity_behavioral_data": request.fidelity_behavioral_data,

        "physiological_data": request.physiological_data,
        "recording_time_physiological_data": int(request.recording_time_physiological_data),
        "fidelity_physiological_data": request.fidelity_physiological_data,

    }

    return ret


def micro_parser(micro, study, event_id, lifetime, reminder, question, answer_possibilities, obj):
    j_request = {
        'name': micro.name,
        'study': study.getIdentifier(),
        'isSurvey': True,
        'event_id': event_id,
        'type': 'microSurvey',
        'timeBuffer': micro.timeBuffer,
        'timeBufferDirection': micro.timeBufferDirection,
        'schedule': micro.schedule,
        'questionLifetime': lifetime,
        'remindOnceAfter': reminder,
        'question': question.question,
        'answers': answer_possibilities,
        'microsurvey_question_id': question.id,
        'microsurvey_answers_id': obj.id,
        'microsurvey_id': micro.id,
    }

    return j_request


def get_micro_survey_parsed(study):
    questions = []

    for uSurvey in MicroSurvey.objects.filter(study=study):
        for question in MicroSurveyQuestion.objects.filter(microsurvey=uSurvey):
            answers = MicroSurveyAnswers.objects.filter(microsurveyquestion=question)[0]
            questions.append(json.dumps(
                {
                    'question': question.question,
                    'first_answer': answers.first_answer,
                    'second_answer': answers.second_answer,
                    'third_answer': answers.third_answer,
                    'microsurvey_question_id': question.id,
                    'microsurvey_answers_id': answers.id,
                }
            ))

        break

    return {
        'name': uSurvey.name,
        'study': study.getIdentifier(),
        'isSurvey': True,
        'type': 'microSurvey',
        'timeBuffer': uSurvey.timeBuffer,
        'timeBufferDirection': uSurvey.timeBufferDirection,
        'questionLifetime': uSurvey.questionLifetime,
        'remindOnceAfter': uSurvey.remindOnceAfter,
        'survey_questions': questions,
        'microsurvey_id': uSurvey.id,
    }
