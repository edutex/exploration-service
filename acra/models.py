from django.db import models
from django.utils.timezone import utc
from datetime import datetime
import json

REPORT_STATUS = (
	("solved","Solved"),
	("unsolved","Unsolved"),
)

class CrashReport(models.Model):
	stack_trace = models.TextField(default="", null=True)
	logcat = models.TextField(default="", null=True)
	shared_preferences = models.TextField(default="", null=True)
	environment = models.TextField(default="", null=True)
	total_mem_size = models.BigIntegerField(default=0,verbose_name='Total Memory Size', null=True)
	initial_configuration = models.TextField(default="", null=True)
	display = models.TextField(default="", null=True)
	available_mem_size = models.BigIntegerField(default=0,verbose_name='Available Memory Size', null=True)
	phone_model = models.CharField(max_length=50,default="", null=True)
	user_comment = models.TextField(default="", null=True)
	crash_configuration = models.TextField(default="", null=True)
	device_features = models.TextField(default="", null=True)
	settings_system = models.TextField(default="",verbose_name='System Settings', null=True)
	file_path = models.CharField(max_length=100,default="", null=True)
	installation_id = models.CharField(max_length=100,default="", null=True)
	user_crash_date = models.CharField(max_length=50,default="",verbose_name='Crash Date', null=True)
	app_version_name = models.CharField(max_length=50,default="",verbose_name='Version Name', null=True)
	user_app_start_date = models.CharField(max_length=50,default="",verbose_name='Application Start Date', null=True)
	settings_global = models.TextField(default="",verbose_name='Global Settings', null=True)
	build = models.TextField(default="", null=True)
	settings_secure = models.TextField(default="",verbose_name='Secure Settings', null=True)
	dumpsys_meminfo = models.TextField(default="", null=True)
	user_email = models.CharField(max_length=50,default="", null=True)
	report_id = models.CharField(max_length=100,default="", null=True)
	product = models.CharField(max_length=50,default="", null=True)
	package_name = models.CharField(max_length=100,default="",verbose_name='Package Name', null=True)
	brand = models.CharField(max_length=50,default="", null=True)
	android_version = models.CharField(max_length=50,default="", null=True)
	app_version_code = models.CharField(max_length=50,default="",verbose_name='Version Code', null=True)
	is_silent = models.CharField(max_length=50,default="", null=True)
	custom_data = models.TextField(default="", null=True)
	description = models.TextField(default="", null=True)
	solved = models.CharField(max_length=10,choices=REPORT_STATUS,default="unsolved",verbose_name='Status', null=True)
	created = models.DateTimeField(auto_now_add=True, null=True)

	def __str__(self):
		return ('Device: %s %s - Android: %s - Application: %s Version: %s') % (self.brand,self.product,self.android_version,self.app_version_name,self.app_version_code)

	def __unicode__(self):
		return ('Device: %s %s - Android: %s - Application: %s Version: %s') % (self.brand,self.product,self.android_version,self.app_version_name,self.app_version_code)