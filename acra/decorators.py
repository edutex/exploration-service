from django.http import HttpResponse
from functools import wraps
import base64

def http_basic_auth(func):
    @wraps(func)
    def _decorator(request, *args, **kwargs):
        from django.contrib.auth import authenticate, login
        if 'HTTP_AUTHORIZATION' in request.META:
            authmeth, auth = request.META['HTTP_AUTHORIZATION'].split(' ', 1)
            if authmeth.lower() == 'basic':
                auth = base64.b64decode(auth).decode("utf-8")
                username, password = auth.split(':', 1)
                user = authenticate(username=username, password=password)
                if user is not None:
                    if user.has_perm('acra.add_crashreport') and user.has_perm('acra.change_crashreport') and user.has_perm('acra.delete_crashreport'):
                        login(request, user)
                        return func(request, *args, **kwargs)
                    else:
                        print("USER DENIED")
        response = HttpResponse()
        response.status_code = 401
        return response
    return _decorator