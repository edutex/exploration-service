from channels.routing import ProtocolTypeRouter, URLRouter
from channels.auth import AuthMiddlewareStack
from sessionhandler.routing import websocket_urlpatterns
from .middleware import *
from channels.sessions import SessionMiddlewareStack
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': TokenAuthMiddlewareStack(
        URLRouter(
            websocket_urlpatterns
        )
    ),
})
