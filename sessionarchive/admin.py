from django.contrib import admin
from .models import SessionArchive
from import_export.admin import ImportExportModelAdmin
# Register your models here.

class SessionArchiveAdmin(ImportExportModelAdmin):
    pass
admin.site.register(SessionArchive, SessionArchiveAdmin)