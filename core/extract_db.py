from core.extract_helper import *
import json, time, pytz
from datetime import datetime, timezone
import traceback
import logging
from multiprocessing.pool import ThreadPool
import gzip

logger = logging.getLogger(__name__)


def extract_by_study(study_name, compressed=True):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'
    json_file = None
    try:
        json_file = extract_by_study_v3(study_name)
    except Exception as er:
        print(er)
        traceback.print_exc()

    if json_file == None:
        try:
            json_file = extract_by_study_v2(study_name)
            return json_file
        except:
            ret = {
                'name': "Extraction by study from ESM Database",
                'file_generated_date_time': str(u_time),
                'version': 0,
                'error': "Export was not successful",
            }
            return json.dumps(ret, indent=1)

    if compressed:
        gzip_obj = gzip.compress(bytes(json_file, "utf-8"))
        return gzip_obj
    else:
        return json_file
def extract_by_study_v2(study_name):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'
    ret = {
        'name': "Extraction by study from ESM Database",
        'file_generated_date_time': str(u_time),
        'version': 2,
        'studies': [],
    }

    for study in Study.objects.filter(name=study_name):

        identifier_study = str(study.getIdentifier())

        j_study = {
            'name': study.name,
            'id': int(study.getIdentifier()),
            'participants': [],
        }
        for participant in Participant.objects.filter(study=study):
            j_participant = {
                'name': participant.name,
                'id': participant.identifier,
                'registration_date': str(participant.registration_date),
                'sessions': []
            }

            if participant.restricted_usage: continue

            for session in Session.objects.filter(participant=participant, study_name=identifier_study):

                j_session = {
                    'id': session.sessionID,
                    'android_id': session.androidID,
                    'timestamp_start': str(session.start),
                    'timestamp_end': str(session.ending),
                    'micro_surveys': [],
                    'surveys': [],
                    'notifications': [],
                    'sensor_reqs': [],
                }

                for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):
                    answers = micro.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                    answers = [(i, answers[i]) for i in range(len(answers))]

                    j_u_survey = {
                        'id': int(micro.microsurvey_id),
                        'name': micro.name,
                        'type': micro.type,
                        'schedule': int(micro.schedule),
                        'android_id': micro.androidID,
                        'timestamps_question_asked': format_timestamps_asked(micro.timestamp_question_asked),
                        'timestamp_question_answered': micro.timestamp_question_answered,
                        'question': micro.question,
                        'answers': answers,
                        'user_answer': format_user_answers(micro.user_answer, answers)[0],
                        'question_id': int(micro.microsurvey_question_id),
                        'answers_id': int(micro.microsurvey_answer_id),
                    }
                    j_session['micro_surveys'].append(j_u_survey)

                for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):

                    j_survey = {
                        'id': int(survey.survey_id),
                        'name': survey.name,
                        'type': survey.type,
                        'schedule': int(survey.schedule),
                        'android_id': survey.androidID,
                        'timestamp_asked': format_timestamps_asked(survey.timestamp_question_asked),
                        'timestamp_answered': survey.timestamp_question_answered.split(";")[0],
                        'questions': [],

                    }
                    for question in DatabaseSurveyQuestions.objects.filter(survey=survey):

                        answers = question.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                        answers = [(i, answers[i]) for i in range(len(answers))]

                        user_answer = format_user_answers(question.user_answer, answers)
                        if len(user_answer) == 0:
                            user_answer = - 2
                            answers.append((- 2, question.user_answer))
                        else:
                            user_answer = user_answer[0]

                        j_question = {
                            'id': int(question.survey_question_id),
                            'display_type': question.display_type,
                            'display_order': int(question.display_order),
                            'add_answers_allowed': question.add_answers_allowed,
                            'question': question.question,
                            'answers': answers,
                            'user_answer': user_answer,

                        }

                        j_survey['questions'].append(j_question)
                    j_session['surveys'].append(j_survey)

                for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):

                    answers = notification.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                    answers = [(i, answers[i]) for i in range(len(answers))]

                    user_answer = format_user_answers(notification.user_answer, answers)
                    if len(user_answer) == 0:
                        user_answer = - 2
                        answers.append((-2, notification.user_answer))
                    else:
                        user_answer = user_answer[0]

                    j_notification = {
                        'id': int(notification.notification_id),
                        'name': notification.name,
                        'type': notification.type,
                        'schedule': int(notification.schedule),
                        'android_id': notification.androidID,
                        'timestamp_asked': format_timestamps_asked(notification.timestamp_question_asked),
                        'timestamp_answered': notification.timestamp_question_answered.split(";"),
                        'add_answers_allowed': notification.add_answers_allowed,
                        'question': notification.question,
                        'answers': answers,
                        'user_answer': user_answer,
                    }
                    j_session['notifications'].append(j_notification)

                for sensor_req in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
                    j_session['sensor_reqs'].append(format_sensor_request(sensor_req))

                j_participant["sessions"].append(j_session)
            j_study["participants"].append(j_participant)
        ret["studies"].append(j_study)

    return json.dumps(ret, indent=1)
def extract_by_study_v3(study_name):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'

    ret = {
        'name': "Extraction by study from ESM Database",
        'file_generated_date_time': str(u_time),
        'version': 3,
        'studies': [],
    }

    for study in Study.objects.filter(name=study_name):

        identifier_study = str(study.getIdentifier())

        j_study = {
            'name': study.name,
            'id': int(study.getIdentifier()),
            'participants': [],
        }
        for participant in Participant.objects.filter(study=study):
            j_participant = {
                'name': participant.name,
                'id': participant.identifier,
                'registration_date_time': str(
                    participant.registration_date.astimezone(pytz.utc).isoformat()[:-9] + 'Z'),
                'sessions': []
            }

            if participant.restricted_usage: continue

            q_session = Session.objects.filter(participant=participant, study_name=identifier_study)
            results = []
            num_of_threads = len(q_session)
            pool = ThreadPool(num_of_threads)
            for i in range(0, num_of_threads):
                results.append(pool.apply_async(extract_session_thread, (q_session[i],)))

            results = [r.get() for r in results]
            pool.close()
            pool.join()

            for j_session, missing_eventID in results:
                if missing_eventID:
                    print("Event_ID Missing. Performing V2 Export")
                    raise Exception("spam", "eggs")
                else:
                    if not j_session is None:
                        if not j_session["requests"] == []:
                            j_participant["sessions"].append(j_session)
                    else:
                        print("Session json is none")

            j_study["participants"].append(j_participant)
        ret["studies"].append(j_study)
    return json.dumps(ret, indent=1)


def extract_by_participant(participant_identifier, compressed=True):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'
    json_file = None
    try:
        json_file = extract_by_participant_v3(participant_identifier)
    except Exception as er:
        print(er)
        traceback.print_exc()

    if json_file == None:
        try:
            json_file = extract_by_participant_v2(participant_identifier)
            return json_file
        except:
            ret = {
                'name': "Extraction by participant from ESM Database",
                'file_generated_date_time': str(u_time),
                'version': 0,
                'error': "Export was not successful",
            }
            return json.dumps(ret, indent=1)

    if compressed:
        gzip_obj = gzip.compress(bytes(json_file, 'utf-8'))
        return gzip_obj
    else:
        return json_file
def extract_by_participant_v2(part_id):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'
    ret = {
        'name': "Extraction by participant from ESM Database",
        'file_generated_date_time': str(u_time),
        'version': 2,
        'studies': [],
    }

    used_participant = Participant.objects.filter(identifier=part_id)
    if not used_participant: return ret

    used_participant = used_participant[0]
    study_name = used_participant.study.name

    for study in Study.objects.filter(name=study_name):

        identifier_study = str(study.getIdentifier())

        j_study = {
            'name': study.name,
            'id': int(study.getIdentifier()),
            'participants': [],
        }

        j_participant = {
            'name': used_participant.name,
            'id': used_participant.identifier,
            'registration_date': str(used_participant.registration_date),
            'sessions': []
        }
        for session in Session.objects.filter(participant=used_participant, study_name=identifier_study):

            j_session = {
                'id': session.sessionID,
                'android_id': session.androidID,
                'timestamp_start': str(session.start),
                'timestamp_end': str(session.ending),
                'micro_surveys': [],
                'surveys': [],
                'notifications': [],
                'sensor_reqs': [],
            }

            for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):
                answers = micro.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                answers = [(i, answers[i]) for i in range(len(answers))]

                j_u_survey = {
                    'id': int(micro.microsurvey_id),
                    'name': micro.name,
                    'type': micro.type,
                    'schedule': int(micro.schedule),
                    'android_id': micro.androidID,
                    'timestamp_question_asked': format_timestamps_asked(micro.timestamp_question_asked),
                    'timestamp_question_answered': micro.timestamp_question_answered,
                    'question': micro.question,
                    'answers': answers,
                    'user_answer': format_user_answers(micro.user_answer, answers)[0],
                    'question_id': int(micro.microsurvey_question_id),
                    'answers_id': int(micro.microsurvey_answer_id),
                }
                j_session['micro_surveys'].append(j_u_survey)

            for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):

                j_survey = {
                    'id': int(survey.survey_id),
                    'name': survey.name,
                    'type': survey.type,
                    'schedule': int(survey.schedule),
                    'android_id': survey.androidID,
                    'timestamp_asked': format_timestamps_asked(survey.timestamp_question_asked),
                    'timestamp_answered': survey.timestamp_question_answered.split(";")[0],
                    'questions': [],

                }
                for question in DatabaseSurveyQuestions.objects.filter(survey=survey):

                    answers = question.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                    answers = [(i, answers[i]) for i in range(len(answers))]

                    user_answer = format_user_answers(question.user_answer, answers)
                    if len(user_answer) == 0:
                        user_answer = - 2
                        answers.append((- 2, question.user_answer))
                    else:
                        user_answer = user_answer[0]

                    j_question = {
                        'id': int(question.survey_question_id),
                        'display_type': question.display_type,
                        'display_order': int(question.display_order),
                        'add_answers_allowed': question.add_answers_allowed,
                        'question': question.question,
                        'answers': answers,
                        'user_answer': user_answer,

                    }

                    j_survey['questions'].append(j_question)
                j_session['surveys'].append(j_survey)

            for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):

                answers = notification.answers[1:-1].replace("'", "").replace(" ", "").split(",")
                answers = [(i, answers[i]) for i in range(len(answers))]

                user_answer = format_user_answers(notification.user_answer, answers)
                if len(user_answer) == 0:
                    user_answer = - 2
                    answers.append((-2, notification.user_answer))
                else:
                    user_answer = user_answer[0]

                j_notification = {
                    'id': int(notification.notification_id),
                    'name': notification.name,
                    'type': notification.type,
                    'schedule': int(notification.schedule),
                    'android_id': notification.androidID,
                    'timestamp_asked': format_timestamps_asked(notification.timestamp_question_asked),
                    'timestamp_answered': notification.timestamp_question_answered.split(";"),
                    'add_answers_allowed': notification.add_answers_allowed,
                    'question': notification.question,
                    'answers': answers,
                    'user_answer': user_answer,
                }
                j_session['notifications'].append(j_notification)

            for sensor_req in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
                j_session['sensor_reqs'].append(format_sensor_request(sensor_req))

            j_participant["sessions"].append(j_session)
        j_study["participants"].append(j_participant)
        ret["studies"].append(j_study)

    return json.dumps(ret, indent=1)
def extract_by_participant_v3(part_id):
    u_time = datetime.utcnow().isoformat()[:-3] + 'Z'

    ret = {
        'name': "Extraction by study from ESM Database",
        'file_generated_date_time': str(u_time),
        'version': 3,
        'studies': [],
    }

    used_participant = Participant.objects.filter(identifier=part_id)
    if not used_participant: return ret
    used_participant = used_participant[0]
    study_name = used_participant.study.name

    for study in Study.objects.filter(name=study_name):

        identifier_study = str(study.getIdentifier())

        j_study = {
            'name': study.name,
            'id': int(study.getIdentifier()),
            'participants': [],
        }

        j_participant = {
            'name': used_participant.name,
            'id': used_participant.identifier,
            'registration_date_time': str(
                used_participant.registration_date.astimezone(pytz.utc).isoformat()[:-9] + 'Z'),
            'sessions': []
        }

        q_session = Session.objects.filter(participant=used_participant, study_name=identifier_study)
        results = []
        num_of_threads = len(q_session)
        pool = ThreadPool(num_of_threads)
        for i in range(0, num_of_threads):
            results.append(pool.apply_async(extract_session_thread, (q_session[i],)))

        results = [r.get() for r in results]
        pool.close()
        pool.join()

        for j_session, missing_eventID in results:
            if missing_eventID:
                print("Event_ID Missing. Performing V2 Export")
                raise Exception("spam", "eggs")
            else:
                if not j_session is None:
                    if not j_session["requests"] == []:
                        j_participant["sessions"].append(j_session)
                else:
                    print("Session json is none")

        # for session in Session.objects.filter(participant=used_participant, study_name=identifier_study):
        #
        #     j_session = {
        #         'id': session.sessionID,
        #         'android_id': session.androidID,
        #         'start_date_time': str(session.start),
        #         'end_date_time': str(session.ending),
        #         'requests': [],
        #     }
        #
        #     # 1. Finding all event_id's
        #     event_ids = []
        #     for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):
        #         if micro.event_id == None:
        #             raise Exception("spam", "eggs")
        #         event_ids.append(micro.event_id)
        #
        #     for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):
        #         if survey.event_id == None:
        #             raise Exception("spam", "eggs")
        #         event_ids.append(survey.event_id)
        #
        #     for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):
        #         if notification.event_id == None:
        #             raise Exception("spam", "eggs")
        #         event_ids.append(notification.event_id)
        #
        #     for sensor in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
        #         if sensor.event_id == None:
        #             raise Exception("spam", "eggs")
        #         event_ids.append(sensor.event_id)
        #
        #     event_ids = list(set(event_ids))
        #
        #     for event_id in event_ids:
        #         tmp = {
        #             "event_id": event_id,
        #             "surveys": {},
        #             "sensorrequests": [],
        #         }
        #         for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):
        #
        #             if micro.event_id == event_id:
        #                 answers = micro.answers[1:-1].replace("'", "").replace(" ", "").split(",")
        #                 answers = [(i, answers[i]) for i in range(len(answers))]
        #
        #                 j_u_survey = {
        #                     'id': int(micro.microsurvey_id),
        #                     'name': micro.name,
        #                     'type': micro.type,
        #                     'schedule': int(micro.schedule),
        #                     'android_id': micro.androidID,
        #                     'timestamps_question_asked': format_timestamps_asked(micro.timestamp_question_asked),
        #                     'timestamp_question_answered': micro.timestamp_question_answered,
        #                     'question': micro.question,
        #                     'answer_options': format_answers_to_dic(answers),
        #                     'user_answer': format_answers_to_dic(format_user_answers(micro.user_answer, answers)),
        #                     'question_id': int(micro.microsurvey_question_id),
        #                     'answers_id': int(micro.microsurvey_answer_id),
        #                 }
        #                 if "microquestions" in tmp["surveys"]:
        #                     tmp['surveys']["microquestions"].append(j_u_survey)
        #                 else:
        #                     tmp["surveys"]["microquestions"] = []
        #                     tmp['surveys']["microquestions"].append(j_u_survey)
        #
        #         for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):
        #
        #             if survey.event_id == event_id:
        #                 j_survey = {
        #                     'id': int(survey.survey_id),
        #                     'name': survey.name,
        #                     'type': survey.type,
        #                     'schedule': int(survey.schedule),
        #                     'android_id': survey.androidID,
        #                     'timestamps_asked': format_timestamps_asked(survey.timestamp_question_asked),
        #                     'timestamp_answered': survey.timestamp_question_answered.split(";")[0],
        #                     'questions': [],
        #
        #                 }
        #                 for question in DatabaseSurveyQuestions.objects.filter(survey=survey):
        #
        #                     answers = question.answers[1:-1].replace("'", "").replace(" ", "").split(",")
        #                     answers = [(i, answers[i]) for i in range(len(answers))]
        #                     user_answer = format_user_answers(question.user_answer, answers)
        #                     if len(user_answer) == 0:
        #                         user_answer = (-2, question.user_answer)
        #                         answers.append(user_answer)
        #                         user_answer = format_answers_to_dic([user_answer])
        #                     else:
        #                         user_answer_index = user_answer[0]
        #                         user_answer = format_answers_to_dic([user_answer_index])
        #
        #                     answers = format_answers_to_dic(answers)
        #
        #                     j_question = {
        #                         'id': int(question.survey_question_id),
        #                         'display_type': question.display_type,
        #                         'display_order': int(question.display_order),
        #                         'add_answers_allowed': question.add_answers_allowed,
        #                         'question': question.question,
        #                         'answers_options': answers,
        #                         'user_answer': user_answer,
        #
        #                     }
        #
        #                     j_survey['questions'].append(j_question)
        #
        #                 if "questionnaires" in tmp["surveys"]:
        #                     tmp['surveys']["questionnaires"].append(j_survey)
        #                 else:
        #                     tmp["surveys"]["questionnaires"] = []
        #                     tmp['surveys']["questionnaires"].append(j_survey)
        #
        #         for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):
        #             if notification.event_id == event_id:
        #
        #                 answers = notification.answers[1:-1].replace("'", "").replace(" ", "").split(",")
        #                 answers = [(i, answers[i]) for i in range(len(answers))]
        #
        #                 user_answer = format_user_answers(str(notification.user_answer), answers)
        #                 if len(user_answer) == 0:
        #                     user_answer = (-2, notification.user_answer)
        #                     answers.append(user_answer)
        #                     user_answer = format_answers_to_dic([user_answer])
        #                 else:
        #                     user_answer_index = user_answer[0]
        #                     user_answer = format_answers_to_dic([user_answer_index])
        #
        #                 answers = format_answers_to_dic(answers)
        #
        #                 j_notification = {
        #                     'id': int(notification.notification_id),
        #                     'name': notification.name,
        #                     'type': notification.type,
        #                     'schedule': int(notification.schedule),
        #                     'android_id': notification.androidID,
        #                     'timestamps_asked': format_timestamps_asked(notification.timestamp_question_asked),
        #                     'timestamp_answered': notification.timestamp_question_answered.split(";"),
        #                     'add_answers_allowed': notification.add_answers_allowed,
        #                     'question': notification.question,
        #                     'answers_options': answers,
        #                     'user_answer': user_answer,
        #                 }
        #
        #                 if "notifications" in tmp["surveys"]:
        #                     tmp['surveys']["notifications"].append(j_notification)
        #                 else:
        #                     tmp["surveys"]["notifications"] = []
        #                     tmp['surveys']["notifications"].append(j_notification)
        #
        #         wifi = []
        #         gps = []
        #         accelerometer = []
        #         microphone = []
        #         light = []
        #         bluetooth = []
        #         heartrate = []
        #         temperature = []
        #
        #         for sensor_req in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
        #             if sensor_req.event_id == event_id:
        #                 if sensor_req.sensor == "microphone":
        #                     if type(sensor_req.values) == dict:
        #                         microphone.append(sensor_req)
        #                 elif sensor_req.sensor == "wifi":
        #                     if type(sensor_req.values) == dict:
        #                         wifi.append(sensor_req)
        #                 elif sensor_req.sensor == "gps":
        #                     if type(sensor_req.values) == dict:
        #                         gps.append(sensor_req)
        #                 elif sensor_req.sensor == "ambientlight":
        #                     if type(sensor_req.values) == dict:
        #                         light.append(sensor_req)
        #                 elif sensor_req.sensor == "temperature":
        #                     if type(sensor_req.values) == dict:
        #                         temperature.append(sensor_req)
        #                 elif sensor_req.sensor == "bluetooth":
        #                     if type(sensor_req.values) == dict:
        #                         bluetooth.append(sensor_req)
        #                 elif sensor_req.sensor == "heart_rate":
        #                     if type(sensor_req.values) == dict:
        #                         heartrate.append(sensor_req)
        #                 elif sensor_req.sensor == "accelerometer":
        #                     if type(sensor_req.values) == dict:
        #                         accelerometer.append(sensor_req)
        #
        #         print(len(light))
        #         print(light)
        #
        #         try:
        #             if len(accelerometer) >= 2:
        #                 zero = accelerometer.pop(0)
        #                 for i in accelerometer:
        #                     zero.values["events"] += i.values["events"]
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(accelerometer) == 1:
        #                 tmp2 = format_sensor_request(accelerometer[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             if len(wifi) >= 2:
        #                 zero = wifi.pop(0)
        #                 for i in wifi:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(wifi) == 1:
        #                 tmp2 = format_sensor_request(wifi[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #
        #             if len(gps) >= 2:
        #                 zero = gps.pop(0)
        #                 for i in gps:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(gps) == 1:
        #                 tmp2 = format_sensor_request(gps[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             logger.debug("Inside try for light")
        #             if len(light) >= 2:
        #                 zero = light.pop(0)
        #
        #                 for i in light:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 print(zero)
        #                 tmp2 = format_sensor_request(zero)
        #                 print(tmp2)
        #
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #             elif len(light) == 1:
        #                 print(light[0])
        #                 tmp2 = format_sensor_request(light[0])
        #                 print(tmp2)
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             if len(microphone) >= 2:
        #                 zero = microphone.pop(0)
        #                 for i in microphone:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(microphone) == 1:
        #                 tmp2 = format_sensor_request(microphone[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             if len(bluetooth) >= 2:
        #                 zero = bluetooth.pop(0)
        #                 for i in bluetooth:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(bluetooth) == 1:
        #                 tmp2 = format_sensor_request(bluetooth[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             if len(heartrate) >= 2:
        #                 zero = heartrate.pop(0)
        #                 for i in heartrate:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #
        #             elif len(heartrate) == 1:
        #                 tmp2 = format_sensor_request(heartrate[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         try:
        #             if len(temperature) >= 2:
        #                 zero = temperature.pop(0)
        #                 for i in temperature:
        #                     zero.values["events"] += i.values["events"]
        #
        #                 tmp2 = format_sensor_request(zero)
        #                 if not tmp2.values == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #             elif len(temperature) == 1:
        #                 tmp2 = format_sensor_request(temperature[0])
        #                 if not tmp2["value"] == []:
        #                     tmp['sensorrequests'].append(tmp2)
        #         except Exception as er:
        #             print(er)
        #             traceback.print_exc()
        #
        #         j_session["requests"].append(tmp)
        #
        #     if not j_session["requests"] == []:
        #         j_participant["sessions"].append(j_session)

        j_study["participants"].append(j_participant)
        ret["studies"].append(j_study)
    return json.dumps(ret, indent=1)


def extract_by_session(sessionID, compressed=True):
    session = Session.objects.filter(sessionID=sessionID)
    session_json, missing_eventID = extract_session_thread(session[0])

    ret = {
        "error": "Session is not v3 conform."
    }

    if compressed and not missing_eventID:
        gzip_obj = gzip.compress(bytes(json.dumps(session_json, indent=1), 'utf-8'))
        return gzip_obj
    elif compressed and missing_eventID:
        gzip_obj = gzip.compress(bytes(ret,"utf-8"))
        return gzip_obj
    elif not compressed and not missing_eventID:
        return json_file
    elif not compressed and missing_eventID:
        return ret

def extract_session_thread(session):
    j_session = {
        'id': session.sessionID,
        'android_id': session.androidID,
        'start_date_time': str(session.start),
        'end_date_time': str(session.ending),
        'requests': [],
    }

    event_ids = []
    for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):
        if micro.event_id == None:
            print("Event ID not found. Raising Exception")
            return None, True
            raise Exception("spam", "eggs")
        event_ids.append(micro.event_id)

    for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):
        if survey.event_id == None:
            print("Event ID not found. Raising Exception")
            return None, True
            raise Exception("spam", "eggs")
        event_ids.append(survey.event_id)

    for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):
        if notification.event_id == None:
            print("Event ID not found. Raising Exception")
            raise Exception("spam", "eggs")
        event_ids.append(notification.event_id)

    for sensor in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
        if sensor.event_id == None:
            print("Event ID not found. Raising Exception")
            return None, True
            raise Exception("spam", "eggs")
        event_ids.append(sensor.event_id)

    event_ids = list(set(event_ids))

    tmp = {}
    for event_id in event_ids:
        tmp = {
            "event_id": event_id,
            "surveys": {},
            "sensorrequests": [],
        }
        for micro in MicroSurveyDatabase.objects.filter(sessionID=session.sessionID):

            if micro.event_id == event_id:
                answers = micro.answers[1:-1].replace("'", "").split(",")
                answers = [(i, answers[i]) for i in range(len(answers))]

                j_u_survey = {
                    'id': int(micro.microsurvey_id),
                    'name': micro.name,
                    'type': "micro_questionnaire",
                    'schedule': int(micro.schedule),
                    'android_id': micro.androidID,
                    'timestamps_question_asked': format_timestamps_asked(micro.timestamp_question_asked),
                    'timestamp_question_answered': micro.timestamp_question_answered,
                    'question': micro.question,
                    'answer_options': format_answers_to_dic(answers),
                    'user_answer': format_answers_to_dic(format_user_answers(micro.user_answer, answers)),
                    'question_id': int(micro.microsurvey_question_id),
                    'answers_id': int(micro.microsurvey_answer_id),
                }
                if "microquestions" in tmp["surveys"]:
                    tmp['surveys']["microquestions"].append(j_u_survey)
                else:
                    tmp["surveys"]["microquestions"] = []
                    tmp['surveys']["microquestions"].append(j_u_survey)

        for survey in DatabaseSurvey.objects.filter(sessionID=session.sessionID):

            if survey.event_id == event_id:
                j_survey = {
                    'id': int(survey.survey_id),
                    'name': survey.name,
                    'type': survey.type,
                    'schedule': int(survey.schedule),
                    'android_id': survey.androidID,
                    'timestamps_asked': format_timestamps_asked(survey.timestamp_question_asked),
                    'timestamp_answered': survey.timestamp_question_answered.split(";")[0],
                    'questions': [],

                }
                for question in DatabaseSurveyQuestions.objects.filter(survey=survey):

                    answers = question.answers[1:-1].replace("'", "").split(",")
                    answers = [(i, answers[i]) for i in range(len(answers))]
                    user_answer = format_user_answers(question.user_answer, answers)
                    if len(user_answer) == 0:
                        user_answer = (-2, question.user_answer)
                        answers.append(user_answer)
                        user_answer = format_answers_to_dic([user_answer])
                    else:
                        user_answer_index = user_answer[0]
                        user_answer = format_answers_to_dic([user_answer_index])

                    answers = format_answers_to_dic(answers)

                    j_question = {
                        'id': int(question.survey_question_id),
                        'display_type': question.display_type,
                        'display_order': int(question.display_order),
                        'add_answers_allowed': question.add_answers_allowed,
                        'question': question.question,
                        'answers_options': answers,
                        'user_answer': user_answer,

                    }

                    j_survey['questions'].append(j_question)

                if "questionnaires" in tmp["surveys"]:
                    tmp['surveys']["questionnaires"].append(j_survey)
                else:
                    tmp["surveys"]["questionnaires"] = []
                    tmp['surveys']["questionnaires"].append(j_survey)

        for notification in DatabaseNotification.objects.filter(sessionID=session.sessionID):
            if notification.event_id == event_id:

                answers = notification.answers[1:-1].replace("'", "").split(",")
                answers = [(i, answers[i]) for i in range(len(answers))]

                user_answer = format_user_answers(str(notification.user_answer), answers)
                if len(user_answer) == 0:
                    user_answer = (-2, notification.user_answer)
                    answers.append(user_answer)
                    user_answer = format_answers_to_dic([user_answer])
                else:
                    user_answer_index = user_answer[0]
                    user_answer = format_answers_to_dic([user_answer_index])

                answers = format_answers_to_dic(answers)

                j_notification = {
                    'id': int(notification.notification_id),
                    'name': notification.name,
                    'type': notification.type,
                    'schedule': int(notification.schedule),
                    'android_id': notification.androidID,
                    'timestamps_asked': format_timestamps_asked(notification.timestamp_question_asked),
                    'timestamp_answered': notification.timestamp_question_answered.split(";"),
                    'add_answers_allowed': notification.add_answers_allowed,
                    'question': notification.question,
                    'answers_options': answers,
                    'user_answer': user_answer,
                }

                if "notifications" in tmp["surveys"]:
                    tmp['surveys']["notifications"].append(j_notification)
                else:
                    tmp["surveys"]["notifications"] = []
                    tmp['surveys']["notifications"].append(j_notification)

        wifi = []
        gps = []
        accelerometer = []
        microphone = []
        light = []
        bluetooth = []
        heartrate = []
        temperature = []
        spo2 = []
        sdnn = []

        for sensor_req in DatabaseRequestSensor.objects.filter(sessionID=session.sessionID):
            if sensor_req.event_id == event_id:
                if sensor_req.sensor == "microphone":
                    if type(sensor_req.values) == dict:
                        microphone.append(sensor_req)
                elif sensor_req.sensor == "wifi":
                    if type(sensor_req.values) == dict:
                        wifi.append(sensor_req)
                elif sensor_req.sensor == "gps":
                    if type(sensor_req.values) == dict:
                        gps.append(sensor_req)
                elif sensor_req.sensor == "ambientlight":
                    if type(sensor_req.values) == dict:
                        light.append(sensor_req)
                elif sensor_req.sensor == "temperature":
                    if type(sensor_req.values) == dict:
                        temperature.append(sensor_req)
                elif sensor_req.sensor == "bluetooth":
                    if type(sensor_req.values) == dict:
                        bluetooth.append(sensor_req)
                elif sensor_req.sensor == "heart_rate":
                    if type(sensor_req.values) == dict:
                        heartrate.append(sensor_req)
                elif sensor_req.sensor == "accelerometer":
                    if type(sensor_req.values) == dict:
                        accelerometer.append(sensor_req)
                elif sensor_req.sensor == "oxygen_saturation":
                    if type(sensor_req.values) == dict:
                        spo2.append(sensor_req)
                elif sensor_req.sensor == "heartrate_variability":
                    if type(sensor_req.values) == dict:
                        sdnn.append(sensor_req)

        try:
            if len(accelerometer) >= 2:
                zero = accelerometer.pop(0)
                for i in accelerometer:
                    zero.values["events"] += i.values["events"]
                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(accelerometer) == 1:
                tmp2 = format_sensor_request(accelerometer[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(wifi) >= 2:
                zero = wifi.pop(0)
                for i in wifi:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(wifi) == 1:
                tmp2 = format_sensor_request(wifi[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        try:

            if len(gps) >= 2:
                zero = gps.pop(0)
                for i in gps:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(gps) == 1:
                tmp2 = format_sensor_request(gps[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            logger.debug("Inside try for light")
            if len(light) >= 2:
                zero = light.pop(0)

                for i in light:
                    zero.values["events"] += i.values["events"]
                tmp2 = format_sensor_request(zero)
                print(tmp2)

                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)

            elif len(light) == 1:
                print(light[0])
                tmp2 = format_sensor_request(light[0])
                print(tmp2)
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(microphone) >= 2:
                zero = microphone.pop(0)
                for i in microphone:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(microphone) == 1:
                tmp2 = format_sensor_request(microphone[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)
        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(bluetooth) >= 2:
                zero = bluetooth.pop(0)
                for i in bluetooth:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(bluetooth) == 1:
                tmp2 = format_sensor_request(bluetooth[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)
        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(heartrate) >= 2:
                zero = heartrate.pop(0)
                for i in heartrate:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)

            elif len(heartrate) == 1:
                tmp2 = format_sensor_request(heartrate[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)
        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(temperature) >= 2:
                zero = temperature.pop(0)
                for i in temperature:
                    zero.values["events"] += i.values["events"]

                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(temperature) == 1:
                tmp2 = format_sensor_request(temperature[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)
        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(spo2) >= 2:
                zero = spo2.pop(0)
                for i in spo2:
                    zero.values["events"] += i.values["events"]
                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(spo2) == 1:
                tmp2 = format_sensor_request(spo2[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        try:
            if len(sdnn) >= 2:
                zero = sdnn.pop(0)
                for i in sdnn:
                    zero.values["events"] += i.values["events"]
                tmp2 = format_sensor_request(zero)
                if not tmp2.values == []:
                    tmp['sensorrequests'].append(tmp2)
            elif len(sdnn) == 1:
                tmp2 = format_sensor_request(sdnn[0])
                if not tmp2["value"] == []:
                    tmp['sensorrequests'].append(tmp2)

        except Exception as er:
            print(er)
            traceback.print_exc()

        j_session["requests"].append(tmp)

    return j_session, False
