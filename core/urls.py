from django.contrib import admin
from django.urls import path, include
from .views import index, request_study, request_participant, delete_by_studyname, delete_by_session,\
    toggle_data_permission, delete_user, update_session_extract, request_session

app_name = "core"

urlpatterns = [
    path('extract_study/<str:study_name>/', request_study, name="extract_study"),
    path('extract_participant/<str:participant_name>/', request_participant, name="extract_participant"),
    path('extract_session/<str:sessionID>/',request_session, name="extract_session"),
    path('delete_by_studyname/<str:studyname>/', delete_by_studyname, name="delete_by_studyname"),
    path('delete_by_session/<str:sessionid>/', delete_by_session, name="delete_by_session"),
    path('toogle_data_usage_permission/', toggle_data_permission, name="toggle_data_permission"),
    path('update_session_extract/<str:studyname>/<str:participant_name>/', update_session_extract, name="update_session_extract"),
    path('delete_user/', delete_user, name="delete_user"),
    path('', index, name='index'),
]
