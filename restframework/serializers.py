from rest_framework import serializers
from sessionhandler.models import Study


class StudySerializer(serializers.ModelSerializer):
    class Meta:
        model = Study
        fields = (
            'name',
            'director',
            'description',
            'creation_date',
            'starting_date',
            'expiration_date',
        )